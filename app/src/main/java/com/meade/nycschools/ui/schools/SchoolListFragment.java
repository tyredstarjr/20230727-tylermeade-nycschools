package com.meade.nycschools.ui.schools;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.meade.nycschools.R;
import com.meade.nycschools.model.pojo.SchoolInfo;
import com.meade.nycschools.ui.information.SchoolInfoFragment;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Displays the list of all schools we have data for
 * If I had more time I would allow the user to slide down to refresh the list (allows a network retry if they were not connected at first for example)
 */
public class SchoolListFragment extends Fragment {

    private SchoolListViewModel viewModel;
    private FastScrollRecyclerView recyclerView;
    private SchoolListAdapter adapter;

    public static SchoolListFragment newInstance() {
        return new SchoolListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Might be a better way to do this but ensures that the "Back Button" is not displayed (if for example the came back from the SchoolInfo screen
        setHasOptionsMenu(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_all_schools, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.rv_all_schools);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new SchoolListAdapter(new ArrayList<>(), schoolInfo -> navigateToSchoolInfo(schoolInfo));
        recyclerView.setAdapter(adapter);
        recyclerView.setFastScrollEnabled(true);

        SchoolListViewModel.SchoolListViewModelFactory factory = new SchoolListViewModel.SchoolListViewModelFactory(getActivity());
        viewModel = new ViewModelProvider(this, factory).get(SchoolListViewModel.class);

        viewModel.getAllSchoolsInfo().observe(getViewLifecycleOwner(), this::handleSchoolList);
    }

    private void handleSchoolList(List<SchoolInfo> schoolInfoList) {
        Collections.sort(schoolInfoList, (s1, s2) -> s1.getSchool_name().compareToIgnoreCase(s2.getSchool_name()));
        adapter.updateSchools(schoolInfoList);
    }

    private void navigateToSchoolInfo(SchoolInfo schoolInfo) {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, SchoolInfoFragment.newInstance(schoolInfo.getDbn()))
                .commitNow();
    }
}

