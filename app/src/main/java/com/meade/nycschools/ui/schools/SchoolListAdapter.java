package com.meade.nycschools.ui.schools;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.meade.nycschools.R;
import com.meade.nycschools.model.pojo.SchoolInfo;

import java.util.List;

/**
 * Custom ListView/Adapter to show all the schools, actually quite happy with how this turned out
 */
public class SchoolListAdapter extends RecyclerView.Adapter<SchoolListAdapter.SchoolListViewHolder> {

    private List<SchoolInfo> schools;
    private OnSchoolClickListener listener;

    public SchoolListAdapter(List<SchoolInfo> schools, OnSchoolClickListener listener) {
        this.schools = schools;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SchoolListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_school, parent, false);
        return new SchoolListViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolListViewHolder holder, int position) {
        holder.bind(schools.get(position));
    }

    @Override
    public int getItemCount() {
        return schools.size();
    }

    public void updateSchools(List<SchoolInfo> newSchools) {
        this.schools.clear();
        this.schools.addAll(newSchools);
        notifyDataSetChanged();
    }

    public interface OnSchoolClickListener {
        void onSchoolClick(SchoolInfo schoolInfo);
    }

    public static class SchoolListViewHolder extends RecyclerView.ViewHolder {
        TextView schoolName;
        TextView satScores;
        Button moreInfoButton;
        boolean isExpanded = false;

        public SchoolListViewHolder(@NonNull View itemView, OnSchoolClickListener listener) {
            super(itemView);
            schoolName = itemView.findViewById(R.id.school_name);
            satScores = itemView.findViewById(R.id.sat_scores);
            moreInfoButton = itemView.findViewById(R.id.more_info_button);

            itemView.setOnClickListener(v -> {
                isExpanded = !isExpanded;
                satScores.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
                moreInfoButton.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
            });

            moreInfoButton.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (listener != null && position != RecyclerView.NO_POSITION) {
                    listener.onSchoolClick((SchoolInfo) v.getTag());
                }
            });
        }

        public void bind(SchoolInfo schoolInfo) {
            schoolName.setText(schoolInfo.getSchool_name());
            satScores.setText("Reading: " + schoolInfo.getSatCriticalReadingAvgScore() +
                    "\nMath: " + schoolInfo.getSatMathAvgScore() +
                    "\nWriting: " + schoolInfo.getSatWritingAvgScore());
            moreInfoButton.setTag(schoolInfo);
        }
    }
}



