package com.meade.nycschools.ui.information;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.meade.nycschools.R;
import com.meade.nycschools.model.pojo.SchoolInfo;
import com.meade.nycschools.ui.schools.SchoolListFragment;

/**
 * Displays more information about the schools when selected
 * Probably should have made a custom ListAdapter that would show all data but I limited it for time sake
 */
public class SchoolInfoFragment extends Fragment {
    private static final String DBN_KEY = "dbn_key";
    private SchoolInfoViewModel viewModel;
    private TextView schoolName;
    private TextView satTestTakers;
    private TextView satReadingScore;
    private TextView satMathScore;
    private TextView satWritingScore;
    private TextView email;
    private TextView phoneNumber;
    private TextView website;
    private TextView address;

    public static SchoolInfoFragment newInstance(String dbn) {
        SchoolInfoFragment fragment = new SchoolInfoFragment();

        Bundle args = new Bundle();
        args.putString(DBN_KEY, dbn);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_school_info, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        email = view.findViewById(R.id.school_email);
        phoneNumber = view.findViewById(R.id.school_phone);
        website = view.findViewById(R.id.school_website);
        address = view.findViewById(R.id.school_address);
        schoolName = view.findViewById(R.id.school_name);
        satTestTakers = view.findViewById(R.id.sat_test_takers);
        satReadingScore = view.findViewById(R.id.sat_reading_score);
        satMathScore = view.findViewById(R.id.sat_math_score);
        satWritingScore = view.findViewById(R.id.sat_writing_score);

        String dbn = getArguments().getString(DBN_KEY);

        SchoolInfoViewModel.SchoolInfoViewModelFactory factory = new SchoolInfoViewModel.SchoolInfoViewModelFactory(getActivity().getApplication(), dbn);
        viewModel = new ViewModelProvider(this, factory).get(SchoolInfoViewModel.class);
        viewModel.getSchoolInfo().observe(getViewLifecycleOwner(), this::populateSchoolInfo);
    }

    private void populateSchoolInfo(SchoolInfo schoolInfo) {
        if (schoolInfo != null) {
            email.setText(schoolInfo.getSchool_email());
            phoneNumber.setText(schoolInfo.getPhone_number());
            website.setText(schoolInfo.getWebsite());
            address.setText(schoolInfo.getPrimary_address_line_1());
            schoolName.setText(schoolInfo.getSchool_name());
            satTestTakers.setText(schoolInfo.getNumOfSatTestTakers());
            satReadingScore.setText(schoolInfo.getSatCriticalReadingAvgScore());
            satMathScore.setText(schoolInfo.getSatMathAvgScore());
            satWritingScore.setText(schoolInfo.getSatWritingAvgScore());
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //Hack, should just go up the stack like fragments are meant to but I cannot get it to work for some reason, and I don't have time to debug
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, SchoolListFragment.newInstance())
                .commitNow();
        return true;
    }
}
