package com.meade.nycschools.ui.splash;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.meade.nycschools.model.api.ApiInterface;
import com.meade.nycschools.model.database.SchoolDatabase;
import com.meade.nycschools.model.pojo.SatInfo;
import com.meade.nycschools.model.pojo.SchoolInfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This handles alot of the network communication, I should add more fail/catches and allow for retries
 */
public class SplashViewModel extends ViewModel {
    private MutableLiveData<List<SchoolInfo>> schoolInfoList;
    private static final String BASE_URL = "https://data.cityofnewyork.us/";
    private SchoolDatabase db;
    private final MutableLiveData<Boolean> isDataLoaded = new MutableLiveData<>();

    public SplashViewModel(Context ctx) {
        schoolInfoList = new MutableLiveData<>();
        db = SchoolDatabase.getInstance(ctx);
    }

    public LiveData<List<SchoolInfo>> getSchoolInfoList() {
        return db.schoolInfoDao().getSchoolInfo();
    }

    public void loadSchoolInfo() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<List<SchoolInfo>> call = apiInterface.getSchoolInfo();

        call.enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<List<SchoolInfo>> call, Response<List<SchoolInfo>> response) {
                if (!response.isSuccessful()) {
                    //TODO With more time I would handle this better
                    Log.e("SplashViewModel", "Error: " + response.code());
                    loadSatInfo();
                    return;
                }

                new Thread(() -> {
                    db.schoolInfoDao().insertSchoolInfo(response.body());
                    loadSatInfo();
                }).start();
            }

            @Override
            public void onFailure(Call<List<SchoolInfo>> call, Throwable t) {
                //TODO With more time I would handle this better
                loadSatInfo();
                Log.e("SplashViewModel", "Error fetching data", t);
            }
        });
    }

    private void loadSatInfo() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<List<SatInfo>> call = apiInterface.getSatScores();

        call.enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<List<SatInfo>> call, Response<List<SatInfo>> response) {
                if (!response.isSuccessful()) {
                    Log.e("SplashViewModel", "Error: " + response.code());
                    isDataLoaded.postValue(true);
                    return;
                }

                new Thread(() -> {
                    // Merge SAT info with existing SchoolInfo in database
                    for (SatInfo satInfo : response.body()) {
                        SchoolInfo school = db.schoolInfoDao().getSchoolByDbn(satInfo.getDbn());
                        if (school != null) {
                            school.setNumOfSatTestTakers(satInfo.getNumOfSatTestTakers());
                            school.setSatCriticalReadingAvgScore(satInfo.getSatCriticalReadingAvgScore());
                            school.setSatMathAvgScore(satInfo.getSatMathAvgScore());
                            school.setSatWritingAvgScore(satInfo.getSatWritingAvgScore());
                            db.schoolInfoDao().update(school); // update school with SAT info
                        }
                    }
                    isDataLoaded.postValue(true);
                }).start();
            }

            @Override
            public void onFailure(Call<List<SatInfo>> call, Throwable t) {
                Log.e("SplashViewModel", "Error fetching SAT data", t);
                isDataLoaded.postValue(true);
            }
        });
    }

    public LiveData<Boolean> isDataLoaded() {
        return isDataLoaded;
    }


    public static class SplashViewModelFactory implements ViewModelProvider.Factory {
        private Context context;

        public SplashViewModelFactory(Context context) {
            this.context = context;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            if (modelClass.isAssignableFrom(SplashViewModel.class)) {
                return (T) new SplashViewModel(context);
            }
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
