package com.meade.nycschools.ui.information;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.meade.nycschools.model.database.SchoolDatabase;
import com.meade.nycschools.model.pojo.SchoolInfo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Controls the lookup of what school the user had actually selected
 */
public class SchoolInfoViewModel extends ViewModel {
    private final MutableLiveData<SchoolInfo> schoolInfo = new MutableLiveData<>();

    //Hack because I cannot get the data on the main thread and I cannot return the data properly otherwise
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();

    public SchoolInfoViewModel(Context ctx, String dbn) {
        SchoolDatabase db = SchoolDatabase.getInstance(ctx);
        executorService.execute(() -> {
            SchoolInfo fetchedSchoolInfo = db.schoolInfoDao().getSchoolByDbn(dbn);
            schoolInfo.postValue(fetchedSchoolInfo);
        });
    }

    public LiveData<SchoolInfo> getSchoolInfo() {
        return schoolInfo;
    }

    public static class SchoolInfoViewModelFactory implements ViewModelProvider.Factory {
        private Context ctx;
        private String dbn;

        public SchoolInfoViewModelFactory(Context ctx, String dbn) {
            this.ctx = ctx;
            this.dbn = dbn;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            return (T) new SchoolInfoViewModel(ctx, dbn);
        }
    }
}
