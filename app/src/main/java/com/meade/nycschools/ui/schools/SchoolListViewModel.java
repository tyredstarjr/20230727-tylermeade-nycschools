package com.meade.nycschools.ui.schools;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.meade.nycschools.model.database.SchoolDatabase;
import com.meade.nycschools.model.pojo.SchoolInfo;

import java.util.List;


/**
 * Gathers data from the database for the ListView, as that cannot be done on the mainUI thread
 */
public class SchoolListViewModel extends ViewModel {

    private LiveData<List<SchoolInfo>> allSchoolsInfo;

    public SchoolListViewModel(Context ctx) {
        allSchoolsInfo = SchoolDatabase.getInstance(ctx).schoolInfoDao().getSchoolInfo();
    }

    public LiveData<List<SchoolInfo>> getAllSchoolsInfo() {
        return allSchoolsInfo;
    }

    public static class SchoolListViewModelFactory implements ViewModelProvider.Factory {
        private Context context;

        public SchoolListViewModelFactory(Context context) {
            this.context = context;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            if (modelClass.isAssignableFrom(SchoolListViewModel.class)) {
                return (T) new SchoolListViewModel(context);
            }
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
