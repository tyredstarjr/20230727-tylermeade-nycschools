package com.meade.nycschools.ui.splash;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.meade.nycschools.R;
import com.meade.nycschools.ui.schools.SchoolListFragment;

/**
 * Displays a picture while we gather up data from the internet in the background, pretty happy with how this turned out overall
 */
public class SplashFragment extends Fragment {
    private SplashViewModel splashVM;

    public static SplashFragment newInstance() {
        return new SplashFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        splashVM = new ViewModelProvider(this, new SplashViewModel.SplashViewModelFactory(getActivity().getApplicationContext())).get(SplashViewModel.class);

        splashVM.isDataLoaded().observe(getViewLifecycleOwner(), isLoaded -> {
            if (isLoaded) {
                navigateToAllSchoolsFragment();
            }
        });

        splashVM.loadSchoolInfo();
    }

    private void navigateToAllSchoolsFragment() {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, SchoolListFragment.newInstance())
                .commitNow();
    }
}
