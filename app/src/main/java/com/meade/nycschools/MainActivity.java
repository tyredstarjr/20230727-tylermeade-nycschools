package com.meade.nycschools;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;

import com.meade.nycschools.model.database.SchoolDatabase;
import com.meade.nycschools.ui.splash.SplashFragment;

/**
 * This is the only actual "Activity" we have as I am using fragments and just replace the fragments in the frame as necessary
 * If there was more interdependency between screens I would store some of that information here, didn't find it necessary for this
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, SplashFragment.newInstance())
                    .commitNow();
        }
    }
}