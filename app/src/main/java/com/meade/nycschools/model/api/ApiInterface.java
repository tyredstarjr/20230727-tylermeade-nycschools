package com.meade.nycschools.model.api;

import com.meade.nycschools.model.pojo.SatInfo;
import com.meade.nycschools.model.pojo.SchoolInfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * This is an interface for Retrofit which allows HTTP requests to be made by using annotations on declared methods.
 * If I had more time I would make all the links be "Build Variables" dependent on a GitLab pipeline, but I don't think they offer that with the free version anymore
**/
public interface ApiInterface {
    //This method will get all the information about the schools
    @GET("resource/s3k6-pzi2.json")
    Call<List<SchoolInfo>> getSchoolInfo();

    //This method is for getting SAT scores. Similar to the above method
    @GET("resource/f9bf-2cp4.json")
    Call<List<SatInfo>> getSatScores();
}
