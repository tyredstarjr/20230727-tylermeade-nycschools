package com.meade.nycschools.model.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Had to create this class to store information gathered from the other endpoint as there were too many logical conflicts when only using the SchoolInfo
 * Probably hacky and should inherit from a common parent class but this works with the limit time frame
 */
public class SatInfo {
    @SerializedName("dbn")
    private String dbn;

    @SerializedName("num_of_sat_test_takers")
    private String numOfSatTestTakers;

    @SerializedName("sat_critical_reading_avg_score")
    private String satCriticalReadingAvgScore;

    @SerializedName("sat_math_avg_score")
    private String satMathAvgScore;

    @SerializedName("sat_writing_avg_score")
    private String satWritingAvgScore;

    public SatInfo(String dbn, String numOfSatTestTakers, String satCriticalReadingAvgScore, String satMathAvgScore, String satWritingAvgScore) {
        this.dbn = dbn;
        this.numOfSatTestTakers = numOfSatTestTakers;
        this.satCriticalReadingAvgScore = satCriticalReadingAvgScore;
        this.satMathAvgScore = satMathAvgScore;
        this.satWritingAvgScore = satWritingAvgScore;
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getNumOfSatTestTakers() {
        return numOfSatTestTakers;
    }

    public void setNumOfSatTestTakers(String numOfSatTestTakers) {
        this.numOfSatTestTakers = numOfSatTestTakers;
    }

    public String getSatCriticalReadingAvgScore() {
        return satCriticalReadingAvgScore;
    }

    public void setSatCriticalReadingAvgScore(String satCriticalReadingAvgScore) {
        this.satCriticalReadingAvgScore = satCriticalReadingAvgScore;
    }

    public String getSatMathAvgScore() {
        return satMathAvgScore;
    }

    public void setSatMathAvgScore(String satMathAvgScore) {
        this.satMathAvgScore = satMathAvgScore;
    }

    public String getSatWritingAvgScore() {
        return satWritingAvgScore;
    }

    public void setSatWritingAvgScore(String satWritingAvgScore) {
        this.satWritingAvgScore = satWritingAvgScore;
    }
}

