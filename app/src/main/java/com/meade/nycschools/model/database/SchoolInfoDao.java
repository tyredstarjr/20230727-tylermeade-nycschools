package com.meade.nycschools.model.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.meade.nycschools.model.pojo.SchoolInfo;

import java.util.List;

/**
 * This defines all of the Queries that the rest of the Java classes can interact with the databases through
 */
@Dao
public interface SchoolInfoDao {
    //Might not be the best idea to just replace all the data? Depends on the use case, works here
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSchoolInfo(List<SchoolInfo> schoolInfo);

    @Query("SELECT * FROM school_info")
    LiveData<List<SchoolInfo>> getSchoolInfo();

    @Query("SELECT * FROM school_info WHERE dbn = :dbn LIMIT 1")
    SchoolInfo getSchoolByDbn(String dbn);

    @Update
    void update(SchoolInfo schoolInfo);
}
