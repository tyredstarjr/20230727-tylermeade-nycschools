package com.meade.nycschools.model.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.meade.nycschools.model.pojo.SchoolInfo;

/**
 * This is the structure/outline of the database that will store all the information we get back from the internet
 * Storing this information locally increase responsiveness and minimizes network usage
 */
@Database(entities = {SchoolInfo.class}, version = 4)
public abstract class SchoolDatabase extends RoomDatabase {
    public abstract SchoolInfoDao schoolInfoDao();

    private static volatile SchoolDatabase INSTANCE;

    public static SchoolDatabase getInstance(final Context context) {
        if (INSTANCE == null) {
            synchronized (SchoolDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            SchoolDatabase.class, "school_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}

